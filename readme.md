jQuery Sheets
=========

jQuery Sheets is a stackable overlay plugin. Stacking overlays allows users to see the title of their previous context when a new overlay is opened. Check out the [demo](http://www.nickvanmeter.com/projects/sheets/demo)

Version
----

0.0.1

Usage
--------------

```sh
$(".is-sheet").sheet();
```

Author
--------------
* Nick VanMeter
* [@nickjvm]
* [nickvanmeter.com]

License
----

MIT

[demo]:demo/index.html
[@nickjvm]:http://twitter.com/nickjvm
[nickvanmeter.com]:http://nickvanmeter.com