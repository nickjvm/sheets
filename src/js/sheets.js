(function($){

    MakeSheet = {
        count:0,
        resizeAll:function() {
            $.each(this.sheets,function(i,e) {
                this.doresize();
            })
        },
        sheets:[]
    }

    $.sheet = function(el, options){
        // To avoid scope issues, use 'self' instead of 'this'
        // to reference this class from internal events and functions.
        var self = this;
        
        // Access to jQuery and DOM versions of element
        self.$el = $(el);
        self.el = el;

        self.href = self.$el.attr("href");
        
        self.init = function(){
            self.options = $.extend(self.defaults, options);
            
            if(!self.$el.data('sheet')) {
                // Add a reverse reference to the DOM object
                self.$el.data("sheet", self);

                self.$el.on('click',self.setup);
            }

        };

        self.setup = function(e) {
            e.preventDefault();
            if(MakeSheet.count == 0) {
                $("<div class='sheets-wrapper'/>").appendTo("body");
                $("body").addClass("has-sheets");
            }
            self.addSheet();
            self.$el.data('sheets',true);
        }
       
        self.linkType = function() {
            var href = self.href;
            if(href.substr(0,1)== '#') {
                return "element"
            } else {
                return "ajax"
            }
        };
        
        self.addSheet = function() {
            var $sheetWrapper = $("<div class='sheet-container'><div class='sheet-curtain'></div>").css({
                paddingTop:self.options.positionTop,
                left:self.options.offsetLeft * MakeSheet.count,
                top:self.options.offsetTop * MakeSheet.count
            });
            var $newSheet = $("<div class='sheet'/>").css({
                //paddingLeft:(MakeSheet.count * 30) + 10,
                //maxHeight:$(window).height(),
            }).appendTo($sheetWrapper);
            self.$newSheet = $newSheet;
            if(self.href != '#') {
                if(self.linkType() == 'ajax') {
                    self.content = $(self.$newSheet).load(self.href, function(response, status){
                        MakeSheet.count++;
                        $sheetWrapper.appendTo(".sheets-wrapper");
                        self.$newSheet.css({
                          //  maxHeight:self.maxHeight()
                        });
                        self.bindEvents($sheetWrapper);
                        $(".is-sheet",self.$newSheet).sheet(self.options);
                    });
                } else {
                    self.content = $(self.href).clone().removeClass("sheets-hidden");

                    self.content.appendTo($(self.$newSheet));

                    $sheetWrapper.appendTo(".sheets-wrapper");
                    MakeSheet.count++;

                    self.$newSheet.css({
                        //maxHeight:self.maxHeight()
                    });
                    
                    self.bindEvents($sheetWrapper);
                    $(".is-sheet",self.$newSheet).sheet(self.options);
                }

                MakeSheet.sheets.push(self);

            } else {
                return false;
            }

        }
        
        self.bindEvents = function($sheetWrapper) {
            $sheetWrapper.on("click",function(e) {
                self.removeSheet($sheetWrapper);            
            });

            $sheetWrapper.find(".sheet").on("click",function(e) {
                e.stopPropagation();
            });

            $sheetWrapper.find('.sheet-close').on("click",function(e) {
                self.removeSheet($sheetWrapper);
            }); 
        }
        
        self.maxHeight = function() {
            return $(window).height() - self.$newSheet.offset().top;
        }
        
        self.removeSheet = function($sheetWrapper) {
            $sheetWrapper.remove();
            MakeSheet.count--;
            if(MakeSheet.count == 0) {
                $('.sheets-wrapper').remove();
                $(".has-sheets").removeClass("has-sheets");
            }
        }

        self.markup = "<div class='container'></div>";
        
        self.doresize = function() {
            self.$newSheet.css({
                //maxHeight:self.maxHeight()
            });
        };
        self.defaults = {
            offsetLeft:20,
            offsetTop:50,
            positionTop:50
        }
        
        // Run initializer
        self.init();
    };

    
    $.fn.sheet = function(options){
        $(window).on('resize.sheets',function() {
            MakeSheet.resizeAll();
        });
        return this.each(function(){
            (new $.sheet(this, options));
        });
    };
    
})(jQuery);